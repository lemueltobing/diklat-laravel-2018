<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PeminjamsRequest as StoreRequest;
use App\Http\Requests\PeminjamsRequest as UpdateRequest;

/**
 * Class PeminjamsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PeminjamsCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Peminjams');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/peminjam');
        $this->crud->setEntityNameStrings('peminjam', 'peminjams');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Judul Buku', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'item_id', // Table column which is FK for Customer table
            'entity'=> 'Buku', // Function (method) in Customer model which return transactions
            'attribute' => 'title', // Column which user see in select box from relational table
            'model' => 'App\Models\Peminjams' // Model which contain FK
        ],
        'update/create/both'
        );

        $this->crud->addColumn([
            'label' => 'Nama Buku', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'item_id', // Table column which is FK for Customer table
            'entity'=> 'Buku', // Function (method) in Customer model which return transactions
            'attribute' => 'title', // Column which user see in select box
            'model' => 'App\Models\Peminjams' // Model which contain FK
        ]);



        $this->crud->addField([
            'label' => 'Tanggal Pengembalian',
            'type' => 'datetime_picker',
            'name' => 'time_returned',
        ]);

        $this->crud->addField([
            'label' => 'Tanggal Peminjaman',
            'type' => 'datetime_picker',
            'name' => 'time_added',
        ]);

        $this->crud->addField([
            'label' => 'Tanggal Perubahan',
            'type' => 'datetime_picker',
            'name' => 'time_updated',
        ]);

        $this->crud->addColumn([
            'name' => 'No',
            'type' => 'row_number',
            'label'=> 'No.',
            'orderable'=> false,
        ])->makeFirstColumn();

        $this->crud->enableExportButtons();

        // add asterisk for fields that are required in PeminjamRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
