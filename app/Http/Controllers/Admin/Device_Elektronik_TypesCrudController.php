<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Device_Elektronik_TypesRequest as StoreRequest;
use App\Http\Requests\Device_Elektronik_TypesRequest as UpdateRequest;

/**
 * Class Device_Elektronik_TypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Device_Elektronik_TypesCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Device_Elektronik_Types');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/device_elektronik_types');
        $this->crud->setEntityNameStrings('device_elektronik_type', 'device_elektronik_types');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'No',
            'type' => 'row_number',
            'label'=> 'No.',
            'orderable'=> false,
        ])->makeFirstColumn();

        $this->crud->enableExportButtons();
        


        // add asterisk for fields that are required in Device_Elektronik_TypeRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
