<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BukuRequest as StoreRequest;
use App\Http\Requests\BukuRequest as UpdateRequest;

/**
 * Class BukuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BukuCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Buku');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/buku');
        $this->crud->setEntityNameStrings('buku', 'bukus');
        
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();
        
        //Nambah relasi antar tabel
        $this->crud->addField([
            'label' => 'Pilih Genre', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'genre_ID', // Table column which is FK for Customer table
            'entity'=> 'Buku_genres', // Function (method) in Customer model which return transactions
            'attribute' => 'genre', // Column which user see in select box from relational table
            'model' => 'App\Models\Buku_genres' // Model which contain FK
        ],
        'update/create/both'
        );

        $this->crud->addField([
            'label' => 'Tanggal Penerbitan',
            'type' => 'date_picker',
            'name' => 'published_at',
        ]);

        $this->crud->addColumn([
            'label' => 'Genre', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'genre_ID', // Table column which is FK for Customer table
            'entity'=> 'Buku_genres', // Function (method) in Customer model which return transactions
            'attribute' => 'genre', // Column which user see in select box
            'model' => 'App\Models\Buku_genres' // Model which contain FK
        ]);

        $this->crud->addColumn([
            'name' => 'No',
            'type' => 'row_number',
            'label'=> 'No.',
            'orderable'=> false,
        ])->makeFirstColumn();

        $this->crud->enableExportButtons();
        

        // add asterisk for fields that are required in BukuRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
