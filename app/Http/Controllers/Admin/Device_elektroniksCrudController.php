<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Device_elektroniksRequest as StoreRequest;
use App\Http\Requests\Device_elektroniksRequest as UpdateRequest;

/**
 * Class Device_elektroniksCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Device_elektroniksCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Device_elektroniks');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/device_elektroniks');
        $this->crud->setEntityNameStrings('device_elektroniks', 'device_elektroniks');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Pilih Tipe', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'type_id', // Table column which is FK for Customer table
            'entity'=> 'Device_types', // Function (method) in Customer model which return transactions
            'attribute' => 'type', // Column which user see in select box from relational table
            'model' => 'App\Models\Device_Elektronik_Types' // Model which contain FK
        ],
        'update/create/both'
        );

        $this->crud->addField([
            'label' => 'Tanggal Penerbitan',
            'type' => 'date_picker',
            'name' => 'year',
        ]);


        $this->crud->addColumn([
            'label' => 'Tipe', // Label for HTML form field
            'type'  => 'select',  // HTML element which displaying transactions
            'name'  => 'type_id', // Table column which is FK for Customer table
            'entity'=> 'Device_types', // Function (method) in Customer model which return transactions
            'attribute' => 'type', // Column which user see in select box
            'model' => 'App\Models\Device_Elektronik_Types' // Model which contain FK
        ]);

        $this->crud->addColumn([
            'name' => 'No',
            'type' => 'row_number',
            'label'=> 'No.',
            'orderable'=> false,
        ])->makeFirstColumn();

        $this->crud->enableExportButtons();
        



        // add asterisk for fields that are required in Device_elektroniksRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
