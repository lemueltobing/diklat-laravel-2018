<?php

use Illuminate\Database\Seeder;

class DiklatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*
        DB::table('diklats')->insert([
            'nm_diklat' => 'Laravel Basic',
            'kd_diklat' => 'TI.01',
            'mentor' => 'ABC',
            'wkt_plksnaan_awl'=>'',
            'wkt_plksnaan_akhr'=>''
        ]);
        */


        //dua dimensi array
        DB::table('diklats')->insert([
            [
            'nm_diklat' => 'Laravel Basic',
            'kd_diklat' => 'TI.01',
            'mentor' => 'ABC',
            /*'wkt_plksnaan_awl'=>'',
            'wkt_plksnaan_akhr'=>''*/
            ],
            [
                'nm_diklat' => 'Laravel Advanced',
                'kd_diklat' => 'TI.02A',
                'mentor' => 'ABC',    
            ],
            [
                'nm_diklat' => 'Laravel praktisi',
                'kd_diklat' => 'TI.03P',
                'mentor' => 'ABC',
            ]
        ]);
    }
}
