<?php

use Illuminate\Database\Seeder;

class BackpackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            [
                'name' => 'Lemuel Tobing',
                'email'=>'lemuel.tobing@gmail.com',
                'password'=>'$2y$10$geEfiSO8bZ5/ynWpASqZ1u3HOlOH0Ebczc7D5Au9a9/yQRpVKGWcK',
            ]
        );
    }
}
