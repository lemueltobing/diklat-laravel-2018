<?php

use Illuminate\Database\Seeder;

class BukuGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku_genres')->insert([
            [
                'genre'=> 'IT',
            ],
            [
                'genre'=> 'Novel',
            ],
            [
                'genre'=> 'Politik',
            ],
            [
                'genre'=> 'Agama',
            ],
            [
                'genre'=> 'Otomotif',
            ],
        ]);
    }
}
