<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku')->insert([
            [
                'author' => 'Laravel',
                'title' => 'Laravel Basic',
                'year' => '2018',
                'genre_ID'=> '1',
                /*'wkt_plksnaan_awl'=>'',
                'wkt_plksnaan_akhr'=>''*/
            ],
            [
                'author' => 'JK Rowling',
                'title' => 'Harry Potter',
                'year' => '2010',
                'genre_ID'=> '2',
            ],
            [
                'author' => 'Ismail Marzuki',
                'title' => 'Politik',
                'year' => '2018',
                'genre_ID'=> '3',
            ],
            [
                'author' => 'BPK',
                'title' => 'Pelajaran Agama',
                'year' => '2018',
                'genre_ID'=> '4',              
            ],
            [
                'author' => 'A. B. C.',
                'title' => 'Merakit Mobil ',
                'year' => '2018',
                'genre_ID'=> '5',
            ]
        ]);
    }
}
