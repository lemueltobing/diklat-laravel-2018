<?php

use Illuminate\Database\Seeder;

class DeviceElektronikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektroniks')->insert([
            [
                'type_id' => '1',
                'brand' => 'Samsung',
                'series' => 'Galaxy Note 9',
                'year'=>'2000'
            ],
            [
                'type_id' => '2',
                'brand' => 'Samsung',
                'series' => 'Monophonic',
                'year'=>'2000'
            ],
            [
                'type_id' => '3',
                'brand' => 'Motorolla',
                'series' => 'Hello Moto V3i',
                'year'=>'2000'
            ],
            [
                'type_id' => '4',
                'brand' => 'Chip',
                'series' => 'Hanchip',
                'year'=>'2000'
            ],
            [
                'type_id' => '5',
                'brand' => 'Lenovo',
                'series' => 'Thinkpad',
                'year'=>'2000'
            ],
            [
                'type_id' => '6',
                'brand' => 'Apple',
                'series' => 'Ipad Air Flex',
                'year'=>'2000'
            ],
            [
                'type_id' => '7',
                'brand' => 'Mikrotik',
                'series' => 'RB1100AHxD',
                'year'=>'2000'
            ],
            [
                'type_id' => '8',
                'brand' => 'HP',
                'series' => 'Pro Curve',
                'year'=>'2000'
            ],
            [
                'type_id' => '9',
                'brand' => 'ABB',
                'series' => 'RTU123',
                'year'=>'2000'
            ],
        ]);
    }
}
