<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<!-- BackPack\PageManager -->
<li><a href="{{ url(config('backpack.base.route_prefix').'/page') }}"><i class="fa fa-file-o"></i> <span>Pages</span></a></li>
<!-- Users, Roles Permissions -->
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-book"></i> <span>Buku dan Genre</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href='{{ backpack_url('buku') }}'><i class='fa fa-book'></i> <span>Buku</span></a></li>
        <li><a href='{{ backpack_url('buku_genres') }}'><i class='fa fa-book'></i> <span>Genre Buku</span></a></li>    
    </ul>
  </li>
  <li class="treeview">
    <a href="#"><i class="fa fa-book"></i> <span>Device and Type</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href='{{ backpack_url('device_elektroniks') }}'><i class='fa fa-tag'></i> <span>Device Elektronik</span></a></li>
        <li><a href='{{ backpack_url('device_elektronik_types') }}'><i class='fa fa-gadget'></i> <span>Type Device Elektronik</span></a></li>
    </ul>
 </li>



<li><a href='{{ backpack_url('tag') }}'><i class='fa fa-tag'></i> <span>Tags</span></a></li>
<!-- Backpack Settings -->
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i> <span>Settings</span></a></li>



<li><a href='{{ backpack_url('peminjam') }}'><i class='fa fa-tag'></i> <span>Peminjam</span></a></li>